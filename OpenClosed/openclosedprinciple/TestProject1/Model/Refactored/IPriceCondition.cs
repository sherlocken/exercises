﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommerceProject.Model.Refactored
{
    /// <summary>
    /// defines a rule based on an order item, if it's met, can evaluate for price
    /// </summary>
    public interface IPriceCondition
    {
        bool ConditionMet(OrderItem orderitem);
    }
}
