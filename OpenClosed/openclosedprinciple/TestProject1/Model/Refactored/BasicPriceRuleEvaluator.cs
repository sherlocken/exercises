﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommerceProject.Model.Refactored
{
    public class BasicPriceRuleEvaluator : IPriceRuleEvaluator
    {
        public decimal Evaluate(OrderItem orderItem, Dictionary<IPriceCondition, IPrice> priceConditionsToPrices,decimal currentPrice)
        {
            foreach (IPriceCondition condition in priceConditionsToPrices.Keys)
            {
                if (condition.ConditionMet(orderItem))
                {
                    return priceConditionsToPrices[condition].NewPrice(orderItem,currentPrice);
                }
            }
            throw new Exception("no pricing could be determined for this item");
        }
    }
}
