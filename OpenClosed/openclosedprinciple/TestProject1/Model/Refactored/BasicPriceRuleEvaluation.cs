﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommerceProject.Model.Refactored
{
    public class BasicPriceRuleEvaluation
    {
        private Dictionary<IPriceCondition, IPrice> conditionToPrice;
        private IPriceRuleEvaluator priceRuleEvaluator;

        public BasicPriceRuleEvaluation()
        {
            this.priceRuleEvaluator = new BasicPriceRuleEvaluator();
            this.conditionToPrice = new Dictionary<IPriceCondition, IPrice>(){
                {new EachWidgetCondition(),new FiveQuantityPrice()},
                {new WeightCondition(),new FourPerKilogramPrice()},
                {new SpecialCondition(),new SpecialPrice()},
                {new B4G0AppleCondition(),new BuyFourGetOnePrice()}
            };
        }

        public decimal Total(List<OrderItem> orderItems)
        {
            decimal total = 0m;
            foreach (OrderItem orderItem in orderItems)
            {
                total += this.priceRuleEvaluator.Evaluate(orderItem, conditionToPrice, total);
            }
            return total;
        }
    }
}
