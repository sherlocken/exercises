﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommerceProject.Model.Refactored
{
    public interface IPrice
    {
        decimal NewPrice(OrderItem orderItem,decimal CurrentPrice);
    }
}

