﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommerceProject.Model.Refactored
{
    /// <summary>
    /// Implementors can define an order in which to evaluate IPriceRules
    /// </summary>
    public interface IPriceRuleEvaluator
    {
        decimal Evaluate(OrderItem orderItem, Dictionary<IPriceCondition, IPrice> priceConditionsToPrices,decimal currentPrice);
    }
}
