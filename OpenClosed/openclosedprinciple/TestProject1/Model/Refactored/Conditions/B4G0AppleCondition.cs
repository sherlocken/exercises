﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommerceProject.Model.Refactored
{
    public class B4G0AppleCondition : IPriceCondition
    {
        public bool ConditionMet(OrderItem orderitem)
        {
            return orderitem.Sku.Contains("B4GO_APPLE");
        }
    }
}
