﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommerceProject.Model.Refactored
{
    public class WeightCondition : IPriceCondition
    {
        public bool ConditionMet(OrderItem orderItem)
        {
            return orderItem.Sku.StartsWith("WEIGHT");
        }
    }
}
