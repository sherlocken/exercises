﻿using System.Collections.Generic;

namespace CommerceProject.Model.Refactored
{
    public class Cart
    {
        private readonly List<OrderItem> _items = new List<OrderItem>();

        public IEnumerable<OrderItem> Items
        {
            get { return _items; }
        }

        public string CustomerEmail { get; set; }

        public void Add(OrderItem orderItem)
        {
            _items.Add(orderItem);
        }

        /// <summary>
        /// A price rule could be based on SKU, as they have been, or on quantity
        /// or some future attribute
        ///
        /// </summary>
        /// <returns></returns>
        public decimal TotalAmount()
        {
            BasicPriceRuleEvaluation evaluation = new BasicPriceRuleEvaluation();
            return evaluation.Total(this._items);
        }
    }
}