﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommerceProject.Model.Refactored
{
    class FiveQuantityPrice : IPrice
    {
        public decimal NewPrice(OrderItem orderItem,decimal currentPrice)
        {
           return currentPrice += orderItem.Quantity*5m;
        }
    }
}
