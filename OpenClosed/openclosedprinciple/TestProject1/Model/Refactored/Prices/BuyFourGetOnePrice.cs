﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommerceProject.Model.Refactored
{
    /// <summary>
    /// assuming that for every 4, one is free
    /// 
    /// also, this will not account for the trickier case where additional items of the same SKU 
    /// show up in a separate orderItem
    /// </summary>
    public class BuyFourGetOnePrice : IPrice
    {
        public decimal NewPrice(OrderItem orderItem, decimal CurrentPrice)
        {
            //only nearest multiple of 4 > 4 is charged
            int quantityToChargeFor = orderItem.Quantity;
            if (orderItem.Quantity >= 4)
            {
                quantityToChargeFor = quantityToChargeFor - (orderItem.Quantity % 4);
            }
            return 1m * quantityToChargeFor;
        }
    }
}
