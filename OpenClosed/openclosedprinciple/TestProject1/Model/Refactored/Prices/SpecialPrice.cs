﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommerceProject.Model.Refactored
{
    public class SpecialPrice : IPrice
    {
        public decimal NewPrice(OrderItem orderItem, decimal currentPrice)
        {
            // $0.40 each; 3 for a $1.00
            currentPrice += orderItem.Quantity * .4m;
            int setsOfThree = orderItem.Quantity / 3;
            currentPrice -= setsOfThree * .2m;
            return currentPrice;
        }
    }
}
