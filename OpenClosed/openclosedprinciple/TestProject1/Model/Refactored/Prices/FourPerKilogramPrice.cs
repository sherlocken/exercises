﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommerceProject.Model.Refactored
{
    public class FourPerKilogramPrice : IPrice
    {
        public decimal NewPrice(OrderItem orderItem,decimal currentPrice)
        {
            // quantity is in grams, price is per kg
            return currentPrice+= orderItem.Quantity*4m/1000;
                
        }
    }
}
