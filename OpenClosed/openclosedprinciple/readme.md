#Open/Closed Principle
- Like last time, create a new branch for your work
- Refactor Cart to use the Open/Closed principle
	- original code that needs refactored is in `Model\Cart.cs`
	- `Model\Refactored` has a Cart class you can use to start
	- unit tests are using this refactored cart
- Use the test suite to check your progress	
	- When finished, all tests should pass