﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommerceProject.Model
{
    public interface IPayment
    {
        void Process(Cart cart);
    }
}
