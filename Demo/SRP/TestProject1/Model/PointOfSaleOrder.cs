﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommerceProject.Model
{
    public class PointOfSaleOrder : IOrder
    {
        private IPayment mPayment;
        private Cart mCart;

        public PointOfSaleOrder(Cart cart, IPayment payment)
        {
            mPayment = payment;
            mCart = cart;
        }

        public void checkout()
        {
            mPayment.Process(mCart);
        }
    }
}
