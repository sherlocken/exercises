﻿using CommerceProject.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommerceProject.Model
{
    public class CreditPayment : IPayment
    {
        public string CreditCardNumber { get; set; }

        public string ExpiresMonth { get; set; }

        public string ExpiresYear { get; set; }

        public string CardholderName { get; set; }

        public void Process(Cart cart)
        {
            ChargeCard(cart);
        }

        private void ChargeCard(Cart cart)
        {
            using (var paymentGateway = new PaymentGateway())
            {
                try
                {
                    paymentGateway.Credentials = "account credentials";
                    paymentGateway.CardNumber = CreditCardNumber;
                    paymentGateway.ExpiresMonth = ExpiresMonth;
                    paymentGateway.ExpiresYear = ExpiresYear;
                    paymentGateway.NameOnCard = CardholderName;
                    paymentGateway.AmountToCharge = cart.TotalAmount;

                    paymentGateway.Charge();
                }
                catch (AvsMismatchException ex)
                {
                    throw new OrderException("The card gateway rejected the card based on the address provided.", ex);
                }
                catch (Exception ex)
                {
                    throw new OrderException("There was a problem with your card.", ex);
                }
            }
        }
    }
}
